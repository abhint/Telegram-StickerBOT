import os, time
from os import error
from typing import Text 
from hurry.filesize import size, alternative
from pyrogram import Client, Filters
from bot.helpers.convert.tool import ConvertTool
from bot.helpers.display.display_progress import progress_for_pyrogram

@Client.on_message(Filters.sticker)
async def StickerDownload(client, message):
    c_time = time.time()
    Msg = await message.reply_text(
        text = "Processing your request..."
    )
    location = os.path.dirname(os.path.abspath(__file__))
    download_location = os.path.join(
        location,
        '__DOWNLOAD__',
        str(message.from_user.id)
        )
    download_sticker_location = os.path.join(
        download_location,
        str(message.from_user.id)+'.png'
        )
    download_Animated_location = os.path.join(
        download_location,
        str(message.from_user.id)+'.tgs'
        )
    user_Animated_file = os.path.join(
        download_location,
        str(message.from_user.id)+'.gif'
    )
    if str(message.sticker.file_name) == "sticker.webp":
        try:
            await message.download(
                file_name = download_sticker_location,
                progress=progress_for_pyrogram,
                progress_args=(
                    "Trying to download....",
                    Msg,
                    c_time
                    )
                )
        except Exception as error:
            print(error)
        try:
            await message.reply_photo(
                photo = download_sticker_location
            )
            await message.reply_document(
                document = download_sticker_location,
                caption=f"""<b>Emoji: {message.sticker.emoji}
                \nFile Size: {size(os.stat(download_sticker_location).st_size, system=alternative)}""",
                progress=progress_for_pyrogram,
                progress_args=(
                    "Trying to upload....",
                    Msg,
                    c_time)
                    )
        except Exception as error:
            pass
    elif str(message.sticker.file_name) == "AnimatedSticker.tgs":
        try:
            await message.download(
                file_name = download_Animated_location,
                progress=progress_for_pyrogram,
                progress_args=(
                    "Trying to Download....",
                    Msg,
                    c_time
                    )
                )
        except Exception as error:
            print(error)
        await Msg.edit(
            text = "Converting to gif..."
            )
        await ConvertTool(Msg,download_Animated_location, user_Animated_file)
        try:
            await message.reply_document(
                document = user_Animated_file,
                progress=progress_for_pyrogram,
                progress_args=(
                    "Trying to upload....",
                    Msg,
                    c_time)
                )
        except Exception as error:
            print(error)
        await Msg.edit_text(
            text = f"""<b>Emoji: {message.sticker.emoji}
            \nFile Size: {size(os.stat(user_Animated_file).st_size, system=alternative)}
            </b>""",
            parse_mode = "html"
        )
    else:
        print('error')