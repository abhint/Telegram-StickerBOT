#!/usr/bin/env python3

import re
import sys
import os
import argparse
sys.path.insert(0, os.path.join(
    os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
    "lib"
))
from lottie.exporters import exporters
from lottie.importers import importers
from lottie.utils.stripper import float_strip
from lottie import __version__
import threading
from pyrogram import Client

async def ConvertTool(message, infile, outfile):
    importer = None
    suf = os.path.splitext(infile)[1][1:]
    for p in importers:
        if suf in p.extensions:
            importer = p
    exporter = exporters.get_from_filename(outfile)
    an = importer.process(infile) 
    float_strip(an)
    exporter.process(an, outfile)
    await message.edit(
        text = "Сonversion complete"
        )
    # try:
    #     await os.remove(infile)
    # except Exception as error:
    #     print(error)